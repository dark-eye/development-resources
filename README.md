# Development Resources

This repository is for organizing tasks related to the creation of
devlopment resources for Ubuntu Touch.

Tutorials and resources will be organized in the
UBports documentation at [docs.ubports.com](http://docs.ubports.com/en/latest/appdev/index.html).

Anyone is welcome to join up with the UBports App Development
Steering Committee to help see this project through. We appreciate 
your time and support!

## Issues

The current issues are listed in this repo's
[issue list](https://gitlab.com/ubports/ux/development-resources/issues).

## Discussions

Discussions for this project happne on the
[UBAD Discussion group on Matrix](https://matrix.to/#/!LRhuIUvZsIqZjySTGh:matrix.org).
Everyone is welcome to come and discuss this project!
